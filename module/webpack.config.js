const path = require("path");

module.exports = {
  entry: "../src/AwesomeRetroCutscenes/AwesomeRetroCutscenes.tsx",
  output: {
    path: path.resolve(__dirname, "../dist"),
    filename: "cutscenes.js",
    library: "cutscenes",
    libraryTarget: "commonjs2",
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: "ts-loader",
          options: {
            context: __dirname,
            configFile: path.resolve(__dirname, "tsconfig.json"),
          },
        },
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  optimization: {},
  plugins: [],
  externals: {
    "pixi-filters": {
      commonjs: "pixi-filters",
      commonjs2: "pixi-filters",
      amd: "pixi-filters",
      root: "_",
    },
    "pixi-sound": {
      commonjs: "pixi-sound",
      commonjs2: "pixi-sound",
      amd: "pixi-sound",
      root: "_",
    },
    "pixi.js": {
      commonjs: "pixi.js",
      commonjs2: "pixi.js",
      amd: "pixi.js",
      root: "_",
    },
    react: {
      commonjs: "react",
      commonjs2: "react",
      amd: "react",
      root: "_",
    },
    "react-dom": {
      commonjs: "react-dom",
      commonjs2: "react-dom",
      amd: "react-dom",
      root: "_",
    },
  },
};
