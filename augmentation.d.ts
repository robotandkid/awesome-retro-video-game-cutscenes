import "pixi.js";

declare module "pixi.js" {
  export interface ILoaderResource {
    sound: {
      stop: () => void;
      play: (opts: { loop: boolean }) => void;
    };
  }
}

declare global {
  interface HTMLCanvasElement {
    captureStream(fps: number): MediaStream;
  }

  class MediaRecorder {
    static isTypeSupported(
      mimeType: "video/webm" | "video/mp4" | "video/mp4;codecs=avc1.4d002a"
    );

    constructor(
      stream: MediaStream,
      opts?: { mimeType: "video/mp4" | "video/mp4;codecs=avc1.4d002a" }
    );

    addEventListener(
      name: "dataavailable",
      cb: (props: { data: BlobPart }) => void
    );

    removeEventListener(
      name: "dataavailable",
      cb: (props: { data: BlobPart }) => void
    );

    start();
    stop();
  }
}
