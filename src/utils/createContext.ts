import React from "react";

export function createContext<T>(initial?: T) {
  return React.createContext<T>(initial ?? ({} as T));
}
