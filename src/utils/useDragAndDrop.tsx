import React, { useCallback, useState } from "react";

export type DragState<T> = { originalOrder: T[]; updatedOrder: T[] } & (
  | {
      isDragging: false;
    }
  | {
      isDragging: true;
      draggedFrom: number;
      draggedTo?: number;
    }
);

/**
 * Based on https://codepen.io/florantara/pen/jjyJrZ?editors=0110
 *
 * @param list
 * @param setList
 */
export function useDragAndDrop<T>(list: T[], setList: (list: T[]) => void) {
  const [dragAndDrop, setDragAndDrop] = useState<DragState<T>>({
    isDragging: false,
    originalOrder: [],
    updatedOrder: [],
  });

  const onDragStart = useCallback(
    (event: React.DragEvent<HTMLDivElement>) => {
      const initialPosition = Number(event.currentTarget.dataset.position);

      setDragAndDrop({
        draggedFrom: initialPosition,
        isDragging: true,
        originalOrder: list,
        updatedOrder: [],
      });

      event.dataTransfer.setData("text", `${initialPosition}`);
    },
    [list]
  );

  // onDragOver fires when an element being dragged
  // enters a droppable area.
  // In this case, any of the items on the list
  const onDragOver = useCallback((event: React.DragEvent<HTMLDivElement>) => {
    // in order for the onDrop
    // event to fire, we have
    // to cancel out this one
    event.preventDefault();

    // index of the droppable area being hovered
    const draggedTo = Number(event.currentTarget.dataset.position);

    setDragAndDrop((dnd) => {
      if (!dnd.isDragging) {
        return dnd;
      }

      let newList = dnd.originalOrder;

      // index of the item being dragged
      const draggedFrom = dnd.draggedFrom;

      const itemDragged = newList[draggedFrom!];
      const remainingItems = newList.filter(
        (item, index) => index !== draggedFrom
      );

      newList = [
        ...remainingItems.slice(0, draggedTo),
        itemDragged,
        ...remainingItems.slice(draggedTo),
      ];

      if (draggedTo !== dnd.draggedTo) {
        return {
          ...dnd,
          updatedOrder: newList,
          draggedTo: draggedTo,
        };
      }

      return dnd;
    });
  }, []);

  const onDrop = useCallback(() => {
    setDragAndDrop((dnd) => {
      setList(dnd.updatedOrder);

      return {
        isDragging: false,
        updatedOrder: dnd.updatedOrder,
        originalOrder: dnd.originalOrder,
      };
    });
  }, [setList]);

  const onDragLeave = useCallback(() => {
    setDragAndDrop((dnd) => {
      if (!dnd.isDragging) {
        return dnd;
      }

      return {
        ...dnd,
        draggedTo: undefined,
      };
    });
  }, []);

  return {
    dragAndDrop,
    onDragStart,
    onDragOver,
    onDrop,
    onDragLeave,
  };
}
