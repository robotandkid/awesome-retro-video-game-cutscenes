type F<T> = (props: T) => void;

export function createComponent<T>(hook: F<T>) {
  return (props: T) => {
    hook(props);

    return null;
  };
}
