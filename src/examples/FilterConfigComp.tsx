import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import styled from "styled-components";
import { Col, ConfigHeader } from "../../cosmos/styles";
import { Filter } from "../ui/pixi.js/PixiSprite/Filter.types";
import {
  FilterConfigNumberType,
  FilterConfig,
} from "../ui/pixi.js/PixiSprite/filterConfig";

type BaseFilter = Filter;

function NumberInput(
  props: FilterConfigNumberType & {
    name: string;
    onChange: (value: number) => void;
  }
) {
  const [value, setValue] = useState(props.default);

  return (
    <>
      <div>{props.name}</div>
      <input
        type="number"
        name={props.name}
        min={props.min}
        max={props.max}
        step={props.step}
        onChange={(e) => {
          setValue(+e.target.value);
          props.onChange(+e.target.value);
        }}
        value={value || 0}
      />
    </>
  );
}

function extractOptionsOnly<Filter extends BaseFilter>(
  filter: FilterConfig<Filter>
) {
  return (Object.keys(filter) as (keyof FilterConfig<Filter>)[]).filter(
    (prop) => prop !== "filter" && prop !== "target"
  );
}

export function configToFilter<Filter extends BaseFilter>(
  filter: FilterConfig<Filter>
) {
  return extractOptionsOnly(filter).reduce(
    (total, cur) => ({ ...total, [cur]: filter[cur].default }),
    { filter: filter.filter, target: filter.target } as Filter
  );
}

export function FilterConfigComp<Filter extends BaseFilter>(props: {
  options: FilterConfig<Filter>;
  onOptionsChange: (filter: Filter) => void;
}) {
  const { onOptionsChange: onFilterChange } = props;

  const [optValues, setOptValues] = useState<Filter>(
    configToFilter(props.options)
  );

  const prevValue = useRef<Filter>();

  useEffect(
    function update() {
      if (prevValue.current !== optValues) {
        onFilterChange(optValues);
        prevValue.current = optValues;
      }
    },
    [optValues, onFilterChange, props.options.filter]
  );

  return (
    <Col>
      <ConfigHeader>{props.options.filter}</ConfigHeader>
      {extractOptionsOnly(props.options).map((opt) => {
        return (
          <NumberInput
            default={props.options[opt].default}
            min={props.options[opt].min}
            max={props.options[opt].max}
            step={props.options[opt].step}
            name={opt as string}
            key={opt as string}
            onChange={(value) => {
              setOptValues((values) => ({
                ...values,
                [opt]: value,
              }));
            }}
          />
        );
      })}
    </Col>
  );
}

const FilterContainer = styled.div<{ activateDropTarget?: boolean }>`
  border: 1pt solid #e6dddb;
  background: ${(p) => (p.activateDropTarget ? "#908a89" : "inherit")};
`;

export function DraggableFilterConfigComp<Filter extends BaseFilter>(props: {
  index: number;
  options: FilterConfig<Filter>;
  onOptionsChange: (filter: Filter) => void;
  onSelected: (index: number) => void;
  onDragStart: (evt: React.DragEvent<HTMLDivElement>) => void;
  onDragOver: (evt: React.DragEvent<HTMLDivElement>) => void;
  onDragLeave: (evt: React.DragEvent<HTMLDivElement>) => void;
  onDrop: (evt: React.DragEvent<HTMLDivElement>) => void;
  activateDropTarget?: boolean;
}) {
  const { onSelected, index } = props;

  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    ref.current?.focus();
  }, []);

  return (
    <FilterContainer
      draggable
      ref={ref}
      tabIndex={0}
      style={useMemo(() => ({ border: "1pt solid #E6DDDB" }), [])}
      onFocus={useCallback(() => {
        onSelected(index);
      }, [index, onSelected])}
      onDragStart={props.onDragStart}
      onDragOver={props.onDragOver}
      onDragLeave={props.onDragLeave}
      onDrop={props.onDrop}
      data-position={props.index}
      activateDropTarget={props.activateDropTarget}
    >
      <FilterConfigComp
        options={props.options}
        onOptionsChange={props.onOptionsChange}
      />
    </FilterContainer>
  );
}
