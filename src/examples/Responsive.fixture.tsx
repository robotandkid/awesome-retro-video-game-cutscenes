import React, { useCallback, useState } from "react";
import styled from "styled-components";
import { AwesomeRetroCutscenes } from "../AwesomeRetroCutscenes/AwesomeRetroCutscenes";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default function Fixture() {
  const [size, setSize] = useState(100);

  const toggle = useCallback(() => setSize((s) => (s === 100 ? 200 : 100)), []);

  return (
    <Container>
      <button onClick={toggle}>Toggle size</button>
      <AwesomeRetroCutscenes
        config={{
          background: {
            containerHeight: 0.75 * size,
            containerWidth: size,
          },
          dialog: {
            containerHeight: 0.25 * size,
            containerWidth: size,
            textScroll: {
              fontSize: size === 100 ? 10 : 20,
              fontWeight: "bold",
              fill: "#fff",
            },
          },
        }}
        scenes={[
          {
            title: "start",
            commands: [
              {
                "draw-bg": "/images/duck_attack.jpg",
              },
              {
                say: "hello",
                actor: "narrator",
              },
            ],
          },
        ]}
      />
    </Container>
  );
}
