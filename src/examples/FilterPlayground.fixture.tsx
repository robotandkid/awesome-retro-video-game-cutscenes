import React, { useCallback, useMemo, useState } from "react";
import { Col, OriginalImage, Row } from "../../cosmos/styles";
import { useLoadResourcesForCosmos } from "../../cosmos/useLoadResourcesForCosmos";
import { PixiApp } from "../ui/pixi.js/pixi";
import { Filter as BaseFilter } from "../ui/pixi.js/PixiSprite/Filter.types";
import {
  FilterConfig as BaseConfig,
  FilterConfig,
  filterConfigs,
} from "../ui/pixi.js/PixiSprite/filterConfig";
import { PixiSprite } from "../ui/pixi.js/PixiSprite/PixiSprite";
import { useDragAndDrop } from "../utils/useDragAndDrop";
import { configToFilter, DraggableFilterConfigComp } from "./FilterConfigComp";

type FilterWithId = BaseFilter & {
  id: string;
};

type FilterConfigWithId<Filter extends BaseFilter> = BaseConfig<Filter> & {
  id: string;
};

let id = 0;

function FilterConfigDropdown(props: {
  onAddFilterConfig: (opt: FilterConfigWithId<BaseFilter>) => void;
}) {
  const [filter, setFilter] = useState(filterConfigs[0]);

  return (
    <>
      <select
        value={filter.filter}
        onChange={useCallback((evt) => {
          setFilter(
            filterConfigs.find((opt) => opt.filter === evt.target.value)!
          );
        }, [])}
      >
        {filterConfigs.map((filter) => (
          <option key={filter.filter} value={filter.filter}>
            {filter.filter}
          </option>
        ))}
      </select>
      <button
        onClick={useCallback(() => {
          props.onAddFilterConfig({
            ...filter,
            id: `${++id}`,
          } as FilterConfigWithId<BaseFilter>);
        }, [filter, props])}
      >
        Add filter
      </button>
    </>
  );
}

type FilterSetter = (filters: FilterWithId[]) => FilterWithId[];

function configWithIdToNormalConfig(
  configWithId: FilterConfigWithId<BaseFilter>
) {
  const config = { ...configWithId, id: undefined } as FilterConfig<BaseFilter>;

  delete (config as any).id;

  return config;
}

function Filters(props: { onFilterChange: (cb: FilterSetter) => void }) {
  const { onFilterChange } = props;
  const [configs, setConfigs] = useState<FilterConfigWithId<BaseFilter>[]>([]);
  const [selected, setSelected] = useState<number>();

  const onConfigChange = useCallback(
    (curId: string, curIndex: number) => (filter: BaseFilter) => {
      onFilterChange((_filters) => {
        const curFilters = [..._filters];
        const curFilterAtIndex = curFilters[curIndex];
        const newFilter = { ...filter, id: curId };

        if (curFilterAtIndex?.id !== curId) {
          // add a new filter
          return curFilters.concat([newFilter]);
        }

        curFilters[curIndex] = newFilter;

        return curFilters;
      });
    },
    [onFilterChange]
  );

  const onConfigRemove = useCallback(
    (index: number) => {
      setConfigs((configs) => configs.filter((_, _index) => _index !== index));
      onFilterChange((filters) =>
        filters.filter((_, _index) => _index !== index)
      );
      setSelected(undefined);
    },
    [onFilterChange]
  );

  const configWithoutIds = useMemo(
    () =>
      configs.map((configWithId) => {
        return {
          id: configWithId.id,
          config: configWithIdToNormalConfig(configWithId),
        };
      }),
    [configs]
  );

  const {
    onDragStart,
    onDragOver,
    onDragLeave,
    onDrop,
    dragAndDrop,
  } = useDragAndDrop(
    configs,
    useCallback(
      (newConfigs: FilterConfigWithId<BaseFilter>[]) => {
        setConfigs(newConfigs);
        onFilterChange(() =>
          newConfigs.map((config) => ({
            ...configToFilter(config as any),
            id: config.id,
          }))
        );
      },
      [onFilterChange]
    )
  );

  const dropTargetIndex = dragAndDrop.isDragging
    ? dragAndDrop.draggedTo
    : undefined;

  return (
    <>
      <FilterConfigDropdown
        onAddFilterConfig={useCallback((config) => {
          setConfigs((opts) => [...opts, config]);
        }, [])}
      />
      <button
        disabled={typeof selected === "undefined"}
        onClick={useCallback(() => {
          onConfigRemove(selected!);
        }, [onConfigRemove, selected])}
      >
        Remove filter
      </button>
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {configWithoutIds.map((opt, curIndex) => (
          <DraggableFilterConfigComp<BaseFilter>
            index={curIndex}
            key={opt.id}
            options={opt.config}
            onOptionsChange={onConfigChange(opt.id, curIndex)}
            onSelected={setSelected}
            onDragStart={onDragStart}
            onDragOver={onDragOver}
            onDragLeave={onDragLeave}
            onDrop={onDrop}
            activateDropTarget={curIndex === dropTargetIndex}
          />
        ))}
      </div>
    </>
  );
}

const imgSize = 150;

function Fixture() {
  const [imgUrl, setImgUrl] = useState<string>();

  const onChange = useCallback((evt: React.ChangeEvent<HTMLInputElement>) => {
    setImgUrl(URL.createObjectURL(evt.target.files?.item(0)));
  }, []);

  const [filters, setFilters] = useState<FilterWithId[]>([]);
  const resources = useLoadResourcesForCosmos(
    useMemo(
      () => [
        {
          type: "image",
          path: imgUrl || "/images/strip01.png",
        },
      ],
      [imgUrl]
    )
  );

  return (
    <Col>
      <input type="file" onChange={onChange} accept="image/*" />
      <Row>
        <OriginalImage
          width={imgSize * window.devicePixelRatio}
          height={imgSize * window.devicePixelRatio}
          background="black"
          src={imgUrl || "/images/strip01.png"}
        />
        <PixiApp
          width={imgSize}
          height={imgSize}
          backgroundColor={0x000000}
          resources={resources}
        >
          <PixiSprite
            path={imgUrl || "/images/strip01.png"}
            filter={filters}
            containerWidth={imgSize}
            containerHeight={imgSize}
          />
        </PixiApp>
      </Row>
      <Filters onFilterChange={setFilters} />
    </Col>
  );
}

export default Fixture;
