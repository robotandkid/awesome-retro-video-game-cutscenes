import React, { ReactNode, useContext, useEffect } from "react";
import styled, { keyframes } from "styled-components";
import { LoadResourcesOpts } from "../ui/pixi.js/loaders/useLoadResources";

import {
  AwesomeRetroCutscenes,
  AwesomeRetroCutscenesResourcesContext,
} from "../AwesomeRetroCutscenes/AwesomeRetroCutscenes";

/**
 * "resource loader" that loads an empty resource object
 * and takes 3 seconds.
 */
function FakeDelayedResourceLoader(props: LoadResourcesOpts) {
  useEffect(() => {
    let unsub = setTimeout(() => props.onResourcesChange({}), 3000);

    return () => clearTimeout(unsub);
  }, [props]);

  return null;
}

const Container = styled.div`
  position: relative;
`;

const PlaceHolderContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 0;
  width: ${100 * window.devicePixelRatio}px;
  height: ${100 * window.devicePixelRatio}px;
`;

const InitialPlaceholder = styled.div`
  color: white;
  background: black;
  font-family: sans-serif;
  font-size: 2rem;
  padding: 1rem;
`;

const hourGlass = keyframes`
  0% {
    transform: rotate(0);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }
  50% {
    transform: rotate(900deg);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }
  100% {
    transform: rotate(1800deg);
  }
`;

const LoadingPlaceholder = styled.div`
  display: inline-block;
  position: relative;
  width: 80px;
  height: 80px;

  &:after {
    content: " ";
    display: block;
    border-radius: 50%;
    width: 0;
    height: 0;
    margin: 8px;
    box-sizing: border-box;
    border: 32px solid #fff;
    border-color: #fff transparent #fff transparent;
    animation: ${hourGlass} 1.2s infinite;
  }
`;

function CutscenesFixture(props: {
  loadResourcesOnFocus: boolean;
  children?: ReactNode;
}) {
  return (
    <AwesomeRetroCutscenes
      __loadResourcesComp={FakeDelayedResourceLoader}
      config={{
        loadResourcesOnFocus: props.loadResourcesOnFocus,
        background: {
          containerHeight: 0,
          containerWidth: 0,
        },
        dialog: {
          containerHeight: 100,
          containerWidth: 100,
        },
      }}
      scenes={[
        {
          title: "start",
          commands: [
            {
              say: "test",
              actor: "narrator",
            },
          ],
        },
      ]}
    >
      {props.children}
    </AwesomeRetroCutscenes>
  );
}

function LoadOnClickOrFocusPlaceholder() {
  const resourcesLoaded = useContext(AwesomeRetroCutscenesResourcesContext);

  return (
    <>
      {(resourcesLoaded === "init" || resourcesLoaded === "loading") && (
        <PlaceHolderContainer>
          {resourcesLoaded === "init" && (
            <InitialPlaceholder>Initial Placeholder</InitialPlaceholder>
          )}
          {resourcesLoaded === "loading" && <LoadingPlaceholder />}
        </PlaceHolderContainer>
      )}
    </>
  );
}

function LoadOnClickOrFocusFixture() {
  return (
    <Container>
      <CutscenesFixture loadResourcesOnFocus>
        <LoadOnClickOrFocusPlaceholder />
      </CutscenesFixture>
    </Container>
  );
}

function CustomPlaceholder() {
  const resourcesLoaded = useContext(AwesomeRetroCutscenesResourcesContext);

  return (
    <>
      {resourcesLoaded === "init" ||
        (resourcesLoaded === "loading" && (
          <PlaceHolderContainer>
            <LoadingPlaceholder />
          </PlaceHolderContainer>
        ))}
    </>
  );
}

function CustomPlaceholderFixture() {
  return (
    <Container>
      <CutscenesFixture loadResourcesOnFocus={false}>
        <CustomPlaceholder />
      </CutscenesFixture>
    </Container>
  );
}

export default {
  "click/focus triggers resource load": LoadOnClickOrFocusFixture,
  "custom placeholder until resource loads": CustomPlaceholderFixture,
};
