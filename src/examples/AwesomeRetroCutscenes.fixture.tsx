import React from "react";
import { Col, ConfigHeader, Row } from "../../cosmos/styles";
import { PrettyJson } from "../utils/json";
import { AwesomeRetroCutscenes } from "../AwesomeRetroCutscenes/AwesomeRetroCutscenes";
import { AwesomeRetroCutscenesProps } from "src/AwesomeRetroCutscenes/AwesomeRetroCutscenes.types";

const fixtures: Array<{
  name: string;
  config?: Partial<AwesomeRetroCutscenesProps["config"]>;
  scenes: AwesomeRetroCutscenesProps["scenes"];
}> = [
  {
    name: "basic",
    config: {
      dialog: {
        containerWidth: 200,
        containerHeight: 50,
        textScrollSoundPath: "/sounds/textscroll.wav",
      },
    },
    scenes: [
      {
        title: "start",
        commands: [
          {
            "draw-bg": "/images/duck_attack.jpg",
          },
          {
            actor: "dad",
            say: "watch out son",
          },
          {
            actor: "dad",
            say: "that duck is attacking",
          },
          {
            jump: "scene2",
          },
        ],
      },
      {
        title: "scene2",
        commands: [
          {
            "draw-bg": "/images/jake.jpg",
          },
          {
            actor: "son",
            say: "huh",
          },
        ],
      },
    ],
  },
  {
    name: "with delay",
    scenes: [
      {
        title: "start",
        commands: [
          {
            "draw-bg": "/images/duck_attack.jpg",
          },
          {
            delayInMs: 3000,
          },
          {
            actor: "dad",
            say: "watch out son",
          },
          {
            actor: "dad",
            say: "that duck is attacking",
          },
        ],
      },
    ],
  },
  {
    name: "with filters",
    scenes: [
      {
        title: "start",
        commands: [
          {
            "draw-bg": "/images/duck_attack.jpg",
          },
          {
            filter: "dot",
            scale: 5,
          },
          {
            filter: "oldfilm",
          },
          {
            actor: "dad",
            say: "watch out son",
          },
          {
            actor: "dad",
            say: "that duck is attacking",
          },
        ],
      },
    ],
  },
  {
    name: "sample strip",
    scenes: [
      {
        title: "start",
        commands: [
          {
            "draw-bg": "/images/strip01.png",
          },
          {
            filter: "dot",
            scale: 5,
          },
          {
            filter: "oldfilm",
          },
          {
            actor: "dad",
            say: "watch out son",
          },
          {
            actor: "dad",
            say: "that duck is attacking",
          },
        ],
      },
    ],
  },
  {
    name: "save to video",
    config: {
      background: { containerWidth: 150, containerHeight: 200 },
      dialog: { containerWidth: 0, containerHeight: 0 },
      exportToVideo: true,
    },
    scenes: [
      {
        title: "start",
        commands: [
          {
            "draw-bg": "/images/duck_attack.jpg",
          },
          {
            actor: "dad",
            say: "watch out son",
          },
          {
            actor: "dad",
            say: "that duck is attacking",
          },
          {
            jump: "scene2",
          },
        ],
      },
      {
        title: "scene2",
        commands: [
          {
            "draw-bg": "/images/jake.jpg",
          },
          {
            actor: "son",
            say: "huh",
          },
        ],
      },
    ],
  },
];

const defaultConfig: AwesomeRetroCutscenesProps["config"] = {
  background: { containerWidth: 200, containerHeight: 100 },
  dialog: { containerWidth: 200, containerHeight: 50 },
};

export default fixtures.reduce(
  (total, fixture) => ({
    ...total,
    [fixture.name]: (
      <>
        <AwesomeRetroCutscenes
          scenes={fixture.scenes}
          config={{
            ...defaultConfig,
            ...fixture.config,
          }}
        />
        <Row>
          <Col>
            <ConfigHeader>config</ConfigHeader>
            <PrettyJson
              json={{
                ...defaultConfig,
                ...fixture.config,
              }}
            />
          </Col>
          <Col>
            <ConfigHeader>
              <code>scenes</code>
            </ConfigHeader>
            <PrettyJson json={fixture.scenes} />
          </Col>
        </Row>
      </>
    ),
  }),
  {}
);
