import React, { useCallback, useMemo, useState } from "react";
import { Col, OriginalImage, Row } from "../../cosmos/styles";
import { useLoadResourcesForCosmos } from "../../cosmos/useLoadResourcesForCosmos";
import { CmdFilter } from "../DialogEngine/dialog.types";
import { PixiApp } from "../ui/pixi.js/pixi";
import {
  FilterConfig,
  filterConfigs,
} from "../ui/pixi.js/PixiSprite/filterConfig";
import { PixiSprite } from "../ui/pixi.js/PixiSprite/PixiSprite";
import { PrettyJson } from "../utils/json";
import { FilterConfigComp } from "./FilterConfigComp";

const imageSize = 200;

function Fixture<Filter extends CmdFilter>(props: {
  filterOpts: FilterConfig<Filter>;
}) {
  const [imgUrl, setImgUrl] = useState<string>();

  const onChange = useCallback((evt: React.ChangeEvent<HTMLInputElement>) => {
    setImgUrl(URL.createObjectURL(evt.target.files?.item(0)));
  }, []);

  const [cmd, setCmd] = useState<Filter>();

  const resources = useLoadResourcesForCosmos(
    useMemo(
      () => [
        {
          type: "image",
          path: imgUrl || "/images/strip01.png",
        },
      ],
      [imgUrl]
    )
  );

  return (
    <Col>
      <input type="file" onChange={onChange} accept="image/*" />
      <Row>
        <Col>
          <OriginalImage
            width={imageSize * window.devicePixelRatio}
            height={imageSize * window.devicePixelRatio}
            background="black"
            src={imgUrl || "/images/strip01.png"}
          />
          <FilterConfigComp
            options={props.filterOpts}
            onOptionsChange={setCmd}
          />
        </Col>
        <Col>
          <PixiApp
            width={imageSize}
            height={imageSize}
            backgroundColor={0x000000}
            resources={resources}
          >
            <PixiSprite
              path={imgUrl || "/images/strip01.png"}
              filter={cmd}
              containerWidth={imageSize}
              containerHeight={imageSize}
            />
          </PixiApp>
          {cmd && <PrettyJson json={cmd} />}
        </Col>
      </Row>
    </Col>
  );
}

export default {
  ...filterConfigs.reduce(
    (total, filter) => ({
      ...total,
      [filter.filter]: <Fixture filterOpts={filter} />,
    }),
    {}
  ),
};
