import { Filter } from "../ui/pixi.js/PixiSprite/Filter.types";

export type CmdFilter = Filter;

export type CmdSayLine = {
  say: string;
  actor: "narrator" | string;
};

type CmdDrawBg = {
  "draw-bg": string;
  name?: string;
};

type CmdJump = {
  jump: string;
};

interface CmdDelay {
  delayInMs: number;
}

export type Command = CmdSayLine | CmdDrawBg | CmdJump | CmdFilter | CmdDelay;

/**
 * This is the more user-friendly interface
 */
export interface DialogNode {
  title: string;
  commands: Command[];
}

function isCmdSayLine(cmd: Command): cmd is CmdSayLine {
  return !!(cmd as CmdSayLine).say;
}

function isCmdDrawBg(cmd: Command): cmd is CmdDrawBg {
  return !!(cmd as CmdDrawBg)["draw-bg"];
}

function isCmdJump(cmd: Command): cmd is CmdJump {
  return !!(cmd as CmdJump).jump;
}

function isCmdFilter(cmd: Command): cmd is CmdFilter {
  return !!(cmd as CmdFilter).filter;
}

function isCmdDelay(cmd: Command): cmd is CmdFilter {
  return typeof (cmd as CmdDelay).delayInMs !== "undefined";
}

export type InternalCmdFilter = CmdFilter & { type: "filter" };

export type InternalCommand =
  | (CmdSayLine & { type: "say-line" })
  | (CmdDrawBg & { type: "draw-bg" })
  | (CmdJump & { type: "jump" })
  | (CmdDelay & { type: "delay" })
  | { type: "end" }
  | InternalCmdFilter;

export interface InternalDialogNode {
  title: string;
  commands: InternalCommand[];
}

export function commandToInternalCommand(cmd: Command): InternalCommand {
  if (isCmdSayLine(cmd)) {
    return { ...cmd, type: "say-line" };
  } else if (isCmdDrawBg(cmd)) {
    return { ...cmd, type: "draw-bg" };
  } else if (isCmdFilter(cmd)) {
    return { ...cmd, type: "filter" };
  } else if (isCmdJump(cmd)) {
    return { ...cmd, type: "jump" };
  } else if (isCmdDelay(cmd)) {
    return { ...cmd, type: "delay" };
  } else {
    throw new Error(`unsupported cmd ${cmd}`);
  }
}
