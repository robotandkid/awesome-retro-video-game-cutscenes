import "pixi-sound";
import { Application, Container, ILoaderResource } from "pixi.js";
import React, { useEffect, useRef, useState } from "react";
import { ConvertToVideo } from "../../ConvertToVideo/ConvertToVideo";
import { createContext } from "../../utils/createContext";
import { InjectPixiParent } from "./helpers/InjectPixiParent";
import { LoadPixiApp } from "./loaders/useLoadPixiApp";
import { Resources } from "./loaders/useLoadResources";
import { PixiEventing } from "./PixiEventing";

export interface PixiResourceMetaData {
  type: "image" | "sound";
  path: string;
  /**
   * Must be globally unique
   */
  id?: string;
}

export interface InternalPixiResourceMetaData {
  id: string;
  path: string;
}

// String template types can make this helper unnecessary
export function internalPixiResourceMetaData(
  props: PixiResourceMetaData
): InternalPixiResourceMetaData {
  return {
    id: props.id || `${props.type}:${props.path}`,
    path: props.path,
  };
}

export type PixiNode = React.ReactNode | React.ReactNodeArray;

export interface PixiAppOpts {
  width: number;
  height: number;
  resolution?: number;
  backgroundColor?: number;
  resources: Resources | undefined;
  onAppInit?: () => void;
  onCanvasFocus?: () => void;
  onCanvasClick?: () => void;
  exportToVideo?: boolean;
  children?: PixiNode;
}

interface PixiContextValue {
  appTicker: Application["ticker"] | undefined;
  canvasRef: HTMLCanvasElement | undefined;
  resources: Partial<Record<string, ILoaderResource>>;
}

export const PixiAppTickerContext = createContext<
  PixiContextValue["appTicker"]
>();

export const PixiCanvasRefContext = createContext<
  PixiContextValue["canvasRef"]
>();

export const PixiLoaderResourcesContext = createContext<
  PixiContextValue["resources"] | undefined
>();

export function PixiApp(props: PixiAppOpts) {
  const { onCanvasClick, onCanvasFocus } = props;

  const pixiRef = useRef<HTMLCanvasElement>(null);
  const [application, setApplication] = useState<Application>();

  useEffect(
    function () {
      if (onCanvasClick) {
        window.addEventListener("click", onCanvasClick);
      }

      if (onCanvasFocus) {
        window.addEventListener("focus", onCanvasFocus);
      }

      return () => {
        onCanvasClick && window.removeEventListener("click", onCanvasClick);
        onCanvasFocus && window.removeEventListener("focus", onCanvasFocus);
      };
    },
    [onCanvasClick, onCanvasFocus]
  );

  // So what we do here is that we create an appTicker context
  // so that we can use app.ticker anywhere.
  //
  // But we directly inject this app to its immediate children.
  // This way they can render themselves w/o needing the context
  return (
    <>
      <canvas ref={pixiRef} width={props.width} height={props.height}></canvas>
      <LoadPixiApp {...props} pixiRef={pixiRef} onInitApp={setApplication} />
      <PixiEventing parent={application} />
      <PixiCanvasRefContext.Provider value={application?.view}>
        <ConvertToVideo enabled={!!props.exportToVideo} />
        <PixiAppTickerContext.Provider value={application?.ticker}>
          <PixiLoaderResourcesContext.Provider value={props.resources}>
            <InjectPixiParent parent={application}>
              {props.children}
            </InjectPixiParent>
          </PixiLoaderResourcesContext.Provider>
        </PixiAppTickerContext.Provider>
      </PixiCanvasRefContext.Provider>
    </>
  );
}

export type PixiParent = Application | Container;

export type PixiChild = Container;

/**
 * The PixiApp and PixiContainer will inject themselves
 * to any direct descendants.
 *
 * Add this property if you need access to the pixi parent.
 */
export type PixiElementProps = {
  parent?: PixiParent;
};

export function isApplication(cmp: PixiParent | undefined): cmp is Application {
  return !!(cmp as Application | undefined)?.stage;
}

export function isContainer(cmp: PixiParent | undefined): cmp is Container {
  return !!(cmp as Container | undefined)?.addChild;
}
