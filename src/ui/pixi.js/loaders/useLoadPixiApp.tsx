import { Application, SCALE_MODES, settings } from "pixi.js";
import { RefObject, useEffect, useRef } from "react";
import { createComponent } from "../../../utils/createComponent";
import { PixiAppOpts } from "../pixi";

export function useLoadPixiApp(
  props: PixiAppOpts & {
    pixiRef: RefObject<HTMLCanvasElement | null>;
    onInitApp(app: Application): void;
  }
) {
  const { onInitApp } = props;

  const initCalled = useRef(false);
  const application = useRef<Application>();

  useEffect(
    function resize() {
      if (application.current) {
        application.current.renderer.resize(props.width, props.height);
      }
    },
    [props.height, props.width]
  );

  useEffect(
    function create() {
      if (initCalled.current) {
        return;
      }

      initCalled.current = true;

      const _resolution = props.resolution ?? (window.devicePixelRatio || 1);

      const instance = new Application({
        width: props.width,
        height: props.height,
        backgroundColor: props.backgroundColor ?? 0xffffff,
        resolution: _resolution,
        view: props.pixiRef.current!,
      });

      // Scale mode for all textures, will retain pixelation
      settings.SCALE_MODE = SCALE_MODES.NEAREST;

      // enable key handlers
      instance.view.tabIndex = 0;

      onInitApp(instance);

      application.current = instance;
    },
    [
      onInitApp,
      props.backgroundColor,
      props.height,
      props.pixiRef,
      props.resolution,
      props.width,
    ]
  );
}

export const LoadPixiApp = createComponent(useLoadPixiApp);
