import { Container } from "pixi.js";
import React, { useEffect, useState } from "react";
import { AddToParentOnMount } from "../AddToParentOnMount/AddToParentOnMount";
import { PixiParent, PixiNode } from "../pixi";
import { InjectPixiParent } from "../helpers/InjectPixiParent";

export function PixiContainer(
  props: {
    offsetY?: number;
  } & {
    parent?: PixiParent;
    children?: PixiNode;
  }
) {
  const [parent, setParent] = useState<Container>();

  useEffect(
    function createContainer() {
      const container = parent || new Container();

      if (typeof props.offsetY !== "undefined") {
        container.y = props.offsetY;
      }

      setParent(container);
    },
    [parent, props.offsetY]
  );

  return (
    <>
      <AddToParentOnMount child={parent} parent={props.parent} />
      <InjectPixiParent parent={parent}>{props.children}</InjectPixiParent>
    </>
  );
}
