import { TextStyle } from "pixi.js";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { AppEventing } from "../../../AppEventing/AppEventing";
import { PixiApp } from "../pixi";
import { EventingTypewriterText } from "./TypewriterText";
import { useLoadResourcesForCosmos } from "../../../../cosmos/useLoadResourcesForCosmos";

function Fixture(props: { text: string[] }) {
  const style = useMemo(
    () =>
      new TextStyle({
        fontFamily: "Arial",
        fontSize: 12,
        fontStyle: "italic",
        fontWeight: "bold",
        fill: ["#ffffff", "#00ff99"], // gradient
        stroke: "#4a1850",
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: "#000000",
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
        wordWrap: true,
        wordWrapWidth: 200,
      }),
    []
  );

  const [line, setLine] = useState(0);
  const [lineId, setLineId] = useState(0);

  useEffect(() => {
    console.log("line", line);
  }, [line]);

  const resources = useLoadResourcesForCosmos(
    useMemo(
      () => [
        {
          id: "textScrollId",
          type: "sound",
          path: "/sounds/textscroll.wav",
        },
      ],
      []
    )
  );

  return (
    <>
      <PixiApp
        width={200}
        height={100}
        resolution={window.devicePixelRatio || 1}
        backgroundColor={0x000000}
        resources={resources}
      >
        <EventingTypewriterText
          width={200}
          height={100}
          lineId={`${lineId}`}
          style={style}
          typingSpeedInMs={20}
          onDone={useCallback(() => {
            setLine((index) => {
              if (index < props.text.length - 1) {
                setLineId((c) => c + 1);
                return index + 1;
              }
              return index;
            });
          }, [props.text.length])}
          onDoneDelayMs={500}
        >
          {props.text[line]}
        </EventingTypewriterText>
        <AppEventing
          onPreviousEvent={useCallback(() => {
            setLine(0);
            setLineId((c) => c + 1);
          }, [])}
        />
      </PixiApp>
    </>
  );
}

export default {
  "a single line": <Fixture text={["a single line"]} />,
  "duplicate lines": (
    <Fixture text={["duplicate line", "duplicate line", "last line"]} />
  ),
  "multiple lines": (
    <Fixture
      text={[
        "the first line",
        "the second line",
        "the third line",
        "the fourth line",
        "the fifth line",
      ]}
    />
  ),
};
