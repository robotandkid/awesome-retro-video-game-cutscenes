import { Text, TextStyle } from "pixi.js";
import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { AppEventing } from "../../../AppEventing/AppEventing";
import { AddToParentOnMount } from "../AddToParentOnMount/AddToParentOnMount";
import {
  PixiAppTickerContext,
  PixiElementProps,
  PixiLoaderResourcesContext,
} from "../pixi";

type TypewriterEvent = "type" | "stop";

type TypewriterTextProps = {
  lineId: string;
  children: string;
  event?: TypewriterEvent;
  typingSpeedInMs: number;
  /**
   * Defaults to 0
   */
  onDoneDelayMs?: number;
  onDone?: () => void;
  style: TextStyle;
  width: number;
  height: number;
} & PixiElementProps;

type EventingTypewriterTextProps = Pick<
  TypewriterTextProps,
  | "lineId"
  | "children"
  | "typingSpeedInMs"
  | "onDoneDelayMs"
  | "onDone"
  | "style"
  | "width"
  | "height"
> & {
  autoStart?: boolean;
} & PixiElementProps;

export const textScrollId = "textScrollId";

export function EventingTypewriterText(props: EventingTypewriterTextProps) {
  const { onDone } = props;

  const [event, setEvent] = useState<TypewriterEvent | undefined>(
    props.children ? "type" : "stop"
  );

  const onNextEvent = useCallback(() => {
    setEvent((cmd) => {
      if (cmd === "stop") {
        setTimeout(() => onDone?.());
        return cmd;
      } else {
        return "stop";
      }
    });
  }, [onDone]);

  const onPreviousEvent = useCallback(() => {
    setEvent("type");
  }, []);

  useEffect(
    function whenTextChangesRestart() {
      if (props.children) {
        setEvent(undefined);
      }
    },
    [props.children]
  );

  return (
    <>
      <AppEventing
        onNextEvent={onNextEvent}
        onNextEventDoublePress={onDone}
        onPreviousEvent={onPreviousEvent}
      />
      <TypewriterText {...props} event={event} />
    </>
  );
}

export function useTypeEffect(props: {
  event: TypewriterEvent;
  delayMs: number;
  typeAppTicker: () => void;
}) {
  const { event, typeAppTicker: type } = props;

  const appTicker = useContext(PixiAppTickerContext);
  const currentTick = useRef(0);

  // appTicker runs at 60 frames per second
  // so to get, for example, a typing delay of 500ms
  // you need to run the ticker every 30 frames
  // => 30 frames =  1 / 2 second *  60 frames / second

  const delayInSec = props.delayMs / 1000;
  const tickRate = Math.ceil(delayInSec * 60);

  useEffect(
    function typeEffect() {
      if (!appTicker) {
        return;
      }

      function typeTicker() {
        if (event === "stop") {
          return;
        }

        currentTick.current += 1;

        if (currentTick.current === tickRate) {
          currentTick.current = 0;

          if (event === "type") {
            type();
          }
        }
      }

      if (event === "type") {
        appTicker.add(typeTicker);
      }

      return () => {
        appTicker.remove(typeTicker);
      };
    },
    [appTicker, event, tickRate, type]
  );
}

function usePixiText(style: TextStyle, height: number) {
  return useMemo(() => {
    const pixiCmp = new Text("", style);

    pixiCmp.anchor.set(0.5);
    pixiCmp.x = style.wordWrapWidth / 2;
    pixiCmp.y = height / 2;

    return {
      pixiCmp,
      setPixiText(text: string) {
        pixiCmp.text = text;
      },
    };
  }, [height, style]);
}

/**
 * Types the given text with the given typing speed, and calls onDone when
 * finished.
 *
 * - Change the `event` to `stop`, to jump to the done state.
 * - Change the `event` to `type` to restart typing
 *
 * **Note:** Because of the way React components work, changing the `event` to
 * `type` by itself _twice_ in a row will *not* restart the component _twice_.
 * _In order to achieve this effect, you must also change the lineId_.
 */
export function TypewriterText(props: TypewriterTextProps) {
  const {
    lineId = "0",
    event = "type",
    children: textFromProps = "",
    onDone,
    onDoneDelayMs,
    height,
  } = props;

  const appTicker = useContext(PixiAppTickerContext);
  const resources = useContext(PixiLoaderResourcesContext);
  const [finishedTyping, setFinishedTyping] = useState(false);

  const { pixiCmp, setPixiText } = usePixiText(props.style, height);

  useTypeEffect({
    event,
    delayMs: props.typingSpeedInMs,
    typeAppTicker: useCallback(() => {
      if (pixiCmp.text === textFromProps) {
        setFinishedTyping(true);
        return;
      }

      const newText = textFromProps.substring(0, pixiCmp.text.length + 1);

      setPixiText(newText);
    }, [pixiCmp, setPixiText, textFromProps]),
  });

  useEffect(
    function handleRestartEvent() {
      // changes to any of these props need to trigger a restart
      if (
        textFromProps &&
        typeof lineId !== "undefined" &&
        event === "type" &&
        appTicker
      ) {
        setFinishedTyping(false);
        appTicker.addOnce(() => {
          setPixiText("");
        });
      }
    },
    [appTicker, event, lineId, setPixiText, textFromProps]
  );

  useEffect(
    function handleEndEvent() {
      if (
        textFromProps.includes(pixiCmp.text) &&
        pixiCmp.text !== textFromProps &&
        event === "stop" &&
        appTicker
      ) {
        setFinishedTyping(true);
        appTicker.addOnce(() => {
          setPixiText(textFromProps);
        });
      }
    },
    [appTicker, event, pixiCmp, setPixiText, textFromProps]
  );

  useEffect(
    function handleDoneEvent() {
      let unsubscribe: number;

      // changes that trigger a restart need to kill the onDone timer
      if (
        textFromProps &&
        typeof lineId !== "undefined" &&
        (finishedTyping || event === "stop") &&
        onDone
      ) {
        unsubscribe = setTimeout(() => {
          onDone();
        }, onDoneDelayMs || 0) as any;
      }

      return () => {
        if (typeof unsubscribe !== "undefined") {
          clearTimeout(unsubscribe);
        }
      };
    },
    [event, finishedTyping, lineId, onDone, onDoneDelayMs, textFromProps]
  );

  useEffect(
    function sound() {
      if (finishedTyping || event === "stop") {
        resources?.[textScrollId]?.sound.stop();
      } else if (event === "type") {
        resources?.[textScrollId]?.sound.play({ loop: true });
      }
      return () => {
        resources?.[textScrollId]?.sound.stop();
      };
    },
    [event, finishedTyping, resources]
  );

  useEffect(function turnOffSound() {
    return () => {
      resources?.[textScrollId]?.sound.stop();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <AddToParentOnMount parent={props.parent} child={pixiCmp} />
    </>
  );
}
