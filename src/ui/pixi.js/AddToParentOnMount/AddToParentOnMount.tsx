import { useContext, useEffect } from "react";
import { createComponent } from "../../../utils/createComponent";
import {
  PixiChild,
  PixiParent,
  PixiAppTickerContext,
  isApplication,
} from "../pixi";

function useAddToParentOnMount(props: {
  child: PixiChild | undefined;
  parent: PixiParent | undefined;
}) {
  const appTicker = useContext(PixiAppTickerContext);
  const { child, parent } = props;

  useEffect(
    function addToParent() {
      if (child && parent && appTicker) {
        appTicker.addOnce(() => {
          if (isApplication(parent)) {
            parent.stage.addChild(child);
          } else {
            parent.addChild(child);
          }
        });
      }

      return () => {
        if (!child || !parent || !appTicker) {
          return;
        }

        if (isApplication(parent)) {
          appTicker.addOnce(() => {
            parent.stage.removeChild(child);
          });
        } else {
          appTicker.addOnce(() => {
            parent.removeChild(child);
          });
        }
      };
    },
    [child, parent, appTicker]
  );
}

export const AddToParentOnMount = createComponent(useAddToParentOnMount);
