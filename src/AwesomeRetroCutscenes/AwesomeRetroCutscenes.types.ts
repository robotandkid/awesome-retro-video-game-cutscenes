import { DialogNode } from "../DialogEngine/dialog.types";

export interface TextScrollConfig {
  fontFamily?: string;
  /**
   * @minimum 0
   * @TJS-type integer
   */
  fontSize?: number;
  fontStyle?: "italic" | "normal" | "oblique";
  fontVariant?: "normal" | "small-caps";
  fontWeight?: "normal" | "bold" | "bolder" | "lighter";
  /**
   * - Text color in hex.
   * - If you use more than one color, it becomes a gradient (from top to
   *   bottom).
   */
  fill?: string | string[];
  /**
   * - Draw a stroke around individual characters
   * - Stroke color in hex
   */
  stroke?: string;
  /**
   * @minimum 0
   * @TJS-type integer
   */
  strokeThickness?: number;
  /**
   * Increase/decrease the space between characters.
   *
   * @minimum 0
   * @TJS-type integer
   */
  letterSpacing?: number;
  /**
   * Adds shadow to text.
   */
  dropShadow?: boolean;
  dropShadowAlpha?: number;
  dropShadowAngle?: number;
  /**
   * The blur radius.
   *
   * @TJS-type integer
   */
  dropShadowBlur?: number;
  dropShadowDistance?: number;
  dropShadowColor?: string;
  align?: "center" | "left" | "right";
  /**
   * @minimum 0
   * @TJS-type integer
   */
  lineHeight?: number;
  /**
   * @minimum 0
   * @TJS-type integer
   */
  leading?: number;
  /**
   * @minimum 0
   * @TJS-type integer
   */
  typingSpeedInMs?: number;
}

export interface AwesomeRetroCutscenesProps {
  config: {
    loadResourcesOnFocus?: boolean;
    background: {
      containerWidth: number;
      containerHeight: number;
    };
    dialog: {
      containerWidth: number;
      containerHeight: number;
      textScroll?: TextScrollConfig;
      textScrollSoundPath?: string;
    };
    exportToVideo?: boolean;
  };
  scenes: DialogNode[];
}
