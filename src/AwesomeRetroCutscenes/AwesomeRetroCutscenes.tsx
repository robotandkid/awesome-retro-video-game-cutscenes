import { TextStyle } from "pixi.js";
import React, {
  JSXElementConstructor,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { AppEventing } from "../AppEventing/AppEventing";
import { InternalCmdFilter } from "../DialogEngine/dialog.types";
import {
  DialogEngineCallbackContext,
  DialogEngineCommandContext,
  DialogEngineProvider,
} from "../DialogEngine/DialogEngineProvider";
import {
  LoadResources,
  LoadResourcesOpts,
  Resources,
} from "../ui/pixi.js/loaders/useLoadResources";
import { isContainer, PixiApp, PixiElementProps } from "../ui/pixi.js/pixi";
import { PixiContainer } from "../ui/pixi.js/PixiContainer/PixiContainer";
import { PixiSprite } from "../ui/pixi.js/PixiSprite/PixiSprite";
import { EventingTypewriterText } from "../ui/pixi.js/TypeWriterText/TypewriterText";
import { createContext } from "../utils/createContext";
import {
  AwesomeRetroCutscenesProps,
  TextScrollConfig,
} from "./AwesomeRetroCutscenes.types";
import { useResourcesFromTextScrollConfig } from "./useResourcesFromConfig";

function useCurrentBackground(): [
  string | undefined,
  Array<InternalCmdFilter> | undefined
] {
  const { cmd: currentCommand } = useContext(DialogEngineCommandContext);
  const { advance } = useContext(DialogEngineCallbackContext);

  const [bgPath, setBgPath] = useState<string>();
  const [bgFilter, setBgFilter] = useState<Array<InternalCmdFilter>>([]);

  useEffect(
    function drawBg() {
      if (currentCommand.type === "draw-bg") {
        setBgPath(currentCommand["draw-bg"]);
        advance();
      } else if (currentCommand.type === "filter") {
        setBgFilter((cur) => [...cur, currentCommand]);
        advance();
      }
    },
    [advance, currentCommand]
  );

  return [bgPath, bgFilter];
}

type NodeBackgroundProps = PixiElementProps & {
  width: number;
  height: number;
};

function NodeBackground(props: NodeBackgroundProps) {
  const [path, filter] = useCurrentBackground();

  const container = isContainer(props.parent) ? props.parent : undefined;

  return (
    <PixiSprite
      path={path}
      filter={filter}
      containerHeight={props.height}
      containerWidth={props.width}
      parent={container}
    />
  );
}

function useCurrentDialogLine() {
  const currentCommand = useContext(DialogEngineCommandContext);
  const [dialog, setDialog] = useState<{
    nodeTitle: string;
    cmdIndex: number;
    actor: string;
    line: string;
  }>();

  useEffect(
    function update() {
      if (currentCommand.cmd.type === "say-line") {
        setDialog({
          nodeTitle: currentCommand.nodeTitle,
          cmdIndex: currentCommand.cmdNodeIndex,
          actor: currentCommand.cmd.actor,
          line: currentCommand.cmd.say,
        });
      }
    },
    [currentCommand]
  );

  return dialog;
}

type NodeDialogProps = PixiElementProps & {
  containerHeight: number;
  containerWidth: number;
  textScroll: TextScrollConfig | undefined;
};

function NodeDialog(props: NodeDialogProps) {
  const dialog = useCurrentDialogLine();
  const container = isContainer(props.parent) ? props.parent : undefined;

  const style = useMemo(
    () =>
      new TextStyle({
        ...props.textScroll,
        wordWrap: true,
        wordWrapWidth: props.containerWidth,
      }),
    [props.containerWidth, props.textScroll]
  );

  const { advance } = useContext(DialogEngineCallbackContext);
  const [lineId, setLineId] = useState<string>();

  useEffect(() => {
    setLineId(`${dialog?.nodeTitle}:${dialog?.cmdIndex}`);
  }, [dialog]);

  const lineIndex = useRef(1);

  return (
    <>
      <AppEventing
        onPreviousEvent={useCallback(() => {
          if (dialog?.nodeTitle === "start" && dialog.cmdIndex === 0) {
            // force the dialog to re-render if at the beginning
            setLineId(
              `${dialog.nodeTitle}:${dialog.cmdIndex}:${lineIndex.current++}`
            );
          }
        }, [dialog])}
      />
      {dialog && lineId && (
        <EventingTypewriterText
          lineId={lineId}
          typingSpeedInMs={50}
          style={style}
          parent={container}
          onDone={advance}
          onDoneDelayMs={1000}
          width={props.containerWidth}
          height={props.containerHeight}
        >
          {`${dialog.actor}: ${dialog.line}`}
        </EventingTypewriterText>
      )}
    </>
  );
}

function HandleDelayCommand() {
  const { advance } = useContext(DialogEngineCallbackContext);
  const { cmd: currentCommand } = useContext(DialogEngineCommandContext);

  useEffect(
    function delayedAdvance() {
      let unsub: number;

      if (currentCommand.type === "delay") {
        unsub = setTimeout(advance, currentCommand.delayInMs) as any;
      }

      return () => {
        clearTimeout(unsub);
      };
    },
    [advance, currentCommand]
  );

  return null;
}

function AppEventingWithHandlers() {
  const { advance, restartNodeOrPreviousNode } = useContext(
    DialogEngineCallbackContext
  );
  const { cmd: currentCommand } = useContext(DialogEngineCommandContext);

  const next = useCallback(() => {
    switch (currentCommand.type) {
      case "say-line":
      case "draw-bg":
      case "delay":
        break;
      default:
        advance();
    }
  }, [advance, currentCommand]);

  return (
    <AppEventing
      onPreviousEvent={restartNodeOrPreviousNode}
      onNextEvent={next}
    />
  );
}

interface AwesomeRetroCutscenesContext {
  pixiJS: "init" | "loaded" | "ready";
  resources: "init" | "loading" | "loaded" | "ready";
}

export const AwesomeRetroCutscenesPixiJSContext = createContext<
  AwesomeRetroCutscenesContext["pixiJS"]
>();

export const AwesomeRetroCutscenesResourcesContext = createContext<
  AwesomeRetroCutscenesContext["resources"]
>("init");

function useResourceLifecycle(
  loadResourcesOnFocus: boolean,
  resources: Resources | undefined
): [AwesomeRetroCutscenesContext["resources"], () => void] {
  const [state, setState] = useState<AwesomeRetroCutscenesContext["resources"]>(
    "init"
  );

  useEffect(() => {
    if (!loadResourcesOnFocus) {
      setState((s) => {
        if (s === "init") {
          return "loading";
        }

        return s;
      });
    }
  }, [loadResourcesOnFocus]);

  useEffect(() => {
    if (resources) {
      setState((s) => {
        if (s === "loading") {
          return "loaded";
        }

        return s;
      });
    }
  }, [resources]);

  return [
    state,
    useCallback(function startLoading() {
      setState((s) => {
        if (s === "init") {
          return "loading";
        }

        return s;
      });
    }, []),
  ];
}

function usePixiLifeCycle(): [
  AwesomeRetroCutscenesContext["pixiJS"],
  () => void
] {
  const [state, setState] = useState<AwesomeRetroCutscenesContext["pixiJS"]>(
    "init"
  );

  return [
    state,
    useCallback(function setLoaded() {
      setState("loaded");
    }, []),
  ];
}

function combineState<
  T extends
    | AwesomeRetroCutscenesContext["pixiJS"]
    | AwesomeRetroCutscenesContext["resources"]
>(
  pixi: AwesomeRetroCutscenesContext["pixiJS"],
  resources: AwesomeRetroCutscenesContext["resources"],
  value: T
) {
  return pixi === "loaded" && resources === "loaded" ? "ready" : value;
}

function InternalAwesomeRetroCutscenes(
  props: AwesomeRetroCutscenesProps & {
    __loadResourcesComp: JSXElementConstructor<LoadResourcesOpts> | undefined;
    children?: ReactNode;
  }
) {
  const {
    __loadResourcesComp: LoadResourcesComp = LoadResources,
    config: { loadResourcesOnFocus, background, dialog, exportToVideo },
  } = props;

  const resourcesToLoad = useResourcesFromTextScrollConfig(
    props.config.dialog.textScrollSoundPath
  );

  const [resources, setResources] = useState<Resources>();

  const [pixiState, setPixiState] = usePixiLifeCycle();

  const [resourceState, onFocus] = useResourceLifecycle(
    !!loadResourcesOnFocus,
    resources
  );

  return (
    <>
      {resourceState !== "init" && (
        <LoadResourcesComp
          resourcesToLoad={resourcesToLoad}
          onResourcesChange={setResources}
        />
      )}
      <PixiApp
        width={Math.max(background.containerWidth, dialog.containerWidth)}
        height={background.containerHeight + dialog.containerHeight}
        resolution={window.devicePixelRatio || 1}
        backgroundColor={0x000000}
        resources={resources}
        onAppInit={setPixiState}
        onCanvasFocus={onFocus}
        onCanvasClick={onFocus}
        exportToVideo={!!exportToVideo}
      >
        {resources && <HandleDelayCommand />}
        {resources && <AppEventingWithHandlers />}
        {resources && (
          <PixiContainer>
            <NodeBackground
              width={background.containerWidth}
              height={background.containerHeight}
            />
          </PixiContainer>
        )}
        {resources && (
          <PixiContainer offsetY={background.containerHeight}>
            <NodeDialog
              containerHeight={dialog.containerHeight}
              containerWidth={dialog.containerWidth}
              textScroll={dialog.textScroll}
            />
          </PixiContainer>
        )}
      </PixiApp>
      <AwesomeRetroCutscenesResourcesContext.Provider
        value={combineState(pixiState, resourceState, resourceState)}
      >
        <AwesomeRetroCutscenesPixiJSContext.Provider
          value={combineState(pixiState, resourceState, pixiState)}
        >
          {props.children}
        </AwesomeRetroCutscenesPixiJSContext.Provider>
      </AwesomeRetroCutscenesResourcesContext.Provider>
    </>
  );
}

const textScrollDefaults = () =>
  ({
    fontFamily: "Arial",
    fontSize: 12,
    fontStyle: "italic",
    fontWeight: "bold",
    fill: ["#ffffff", "#00ff99"], // gradient
    stroke: "#4a1850",
    strokeThickness: 5,
    dropShadow: true,
    dropShadowColor: "#000000",
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,
    align: "center",
    typingSpeedInMs: 30,
  } as TextScrollConfig);

export function AwesomeRetroCutscenes(
  props: AwesomeRetroCutscenesProps & {
    __loadResourcesComp?: JSXElementConstructor<LoadResourcesOpts>;
    children?: ReactNode;
  }
) {
  const {
    __loadResourcesComp,
    config: {
      loadResourcesOnFocus,
      dialog: {
        textScroll: {
          align = "center",
          typingSpeedInMs = 30,
          ...textScrollConfig
        } = textScrollDefaults(),
        ...dialogConfig
      } = {},
      background,
      exportToVideo,
    } = {},
    scenes,
  } = props;

  const config = {
    loadResourcesOnFocus,
    dialog: {
      textScroll: {
        align,
        typingSpeedInMs,
        ...textScrollConfig,
      },
      ...dialogConfig,
    },
    background,
    exportToVideo: exportToVideo,
  } as AwesomeRetroCutscenesProps["config"];

  return (
    <DialogEngineProvider nodes={scenes}>
      <InternalAwesomeRetroCutscenes
        config={config}
        scenes={scenes}
        __loadResourcesComp={__loadResourcesComp}
      >
        {props.children}
      </InternalAwesomeRetroCutscenes>
    </DialogEngineProvider>
  );
}
