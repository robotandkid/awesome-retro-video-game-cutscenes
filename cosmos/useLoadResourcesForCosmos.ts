import { useState } from "react";
import {
  LoadResourcesOpts,
  Resources,
  useLoadResources,
} from "../src/ui/pixi.js/loaders/useLoadResources";

export function useLoadResourcesForCosmos(
  resourcesToLoad: LoadResourcesOpts["resourcesToLoad"]
) {
  const [resources, setResources] = useState<Resources>();

  useLoadResources({
    resourcesToLoad,
    onResourcesChange: setResources,
  });

  return resources;
}
