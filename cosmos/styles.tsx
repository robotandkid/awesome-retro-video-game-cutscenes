import React from "react";
import styled from "styled-components";

export const TextH1 = styled.h1`
  color: #565352;
  font-size: "2rem";
  font-family: sans-serif;
`;

export const TextH2 = styled.h2`
  color: #908a89;
  font-size: "1.25rem";
  font-family: sans-serif;
`;

export const ConfigHeader = styled.div`
  color: #908a89;
  padding-top: 1.25rem;
  font-size: 1.25rem;
  font-family: monospace;
  font-weight: bold;
`;

export const Row = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: flex-start;

  @media (max-width: 400px) {
    display: block;
  }
`;

export const Col = styled.div`
  flex: 1;
  margin-right: 1rem;

  &::last-child {
    margin-right: 0;
  }
`;

const StyledImage = styled.img<{ width: number }>`
  width: ${(props) => props.width}px;
  object-fit: cover;
`;

const ImageContainer = styled.div<{
  width: number;
  height: number;
  background: string;
}>`
  width: ${(props) => props.width}px;
  height: ${(props) => props.height}px;
  background: ${(props) => props.background};
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export function OriginalImage(props: {
  height: number;
  width: number;
  background: string;
  src: string;
}) {
  return (
    <ImageContainer
      width={props.width}
      height={props.height}
      background={props.background}
    >
      <StyledImage width={props.width} src={props.src} />
    </ImageContainer>
  );
}
