import React from "react";
export declare function createContext<T>(initial?: T): React.Context<T>;
