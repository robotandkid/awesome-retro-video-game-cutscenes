import React from "react";
export declare type DragState<T> = {
    originalOrder: T[];
    updatedOrder: T[];
} & ({
    isDragging: false;
} | {
    isDragging: true;
    draggedFrom: number;
    draggedTo?: number;
});
/**
 * Based on https://codepen.io/florantara/pen/jjyJrZ?editors=0110
 *
 * @param list
 * @param setList
 */
export declare function useDragAndDrop<T>(list: T[], setList: (list: T[]) => void): {
    dragAndDrop: DragState<T>;
    onDragStart: (event: React.DragEvent<HTMLDivElement>) => void;
    onDragOver: (event: React.DragEvent<HTMLDivElement>) => void;
    onDrop: () => void;
    onDragLeave: () => void;
};
