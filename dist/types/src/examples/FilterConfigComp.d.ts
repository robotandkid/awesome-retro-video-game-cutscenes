import React from "react";
import { Filter } from "../ui/pixi.js/PixiSprite/Filter.types";
import { FilterConfig } from "../ui/pixi.js/PixiSprite/filterConfig";
declare type BaseFilter = Filter;
export declare function configToFilter<Filter extends BaseFilter>(filter: FilterConfig<Filter>): Filter;
export declare function FilterConfigComp<Filter extends BaseFilter>(props: {
    options: FilterConfig<Filter>;
    onOptionsChange: (filter: Filter) => void;
}): JSX.Element;
export declare function DraggableFilterConfigComp<Filter extends BaseFilter>(props: {
    index: number;
    options: FilterConfig<Filter>;
    onOptionsChange: (filter: Filter) => void;
    onSelected: (index: number) => void;
    onDragStart: (evt: React.DragEvent<HTMLDivElement>) => void;
    onDragOver: (evt: React.DragEvent<HTMLDivElement>) => void;
    onDragLeave: (evt: React.DragEvent<HTMLDivElement>) => void;
    onDrop: (evt: React.DragEvent<HTMLDivElement>) => void;
    activateDropTarget?: boolean;
}): JSX.Element;
export {};
