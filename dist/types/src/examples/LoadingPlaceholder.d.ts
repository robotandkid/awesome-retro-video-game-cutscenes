declare function LoadOnClickOrFocusFixture(): JSX.Element;
declare function CustomPlaceholderFixture(): JSX.Element;
declare const _default: {
    "click/focus triggers resource load": typeof LoadOnClickOrFocusFixture;
    "custom placeholder until resource loads": typeof CustomPlaceholderFixture;
};
export default _default;
