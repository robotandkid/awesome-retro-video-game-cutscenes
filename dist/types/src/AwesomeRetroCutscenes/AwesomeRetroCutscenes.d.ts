import React, { JSXElementConstructor, ReactNode } from "react";
import { LoadResourcesOpts } from "../ui/pixi.js/loaders/useLoadResources";
import { AwesomeRetroCutscenesProps } from "./AwesomeRetroCutscenes.types";
export declare const AwesomeRetroCutscenesPixiJSContext: React.Context<"init" | "loaded" | "ready">;
export declare const AwesomeRetroCutscenesResourcesContext: React.Context<"init" | "loaded" | "ready" | "loading">;
export declare function AwesomeRetroCutscenes(props: AwesomeRetroCutscenesProps & {
    __loadResourcesComp?: JSXElementConstructor<LoadResourcesOpts>;
    children?: ReactNode;
}): JSX.Element;
