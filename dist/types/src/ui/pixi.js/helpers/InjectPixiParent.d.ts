import { PixiParent, PixiNode } from "../pixi";
export declare function InjectPixiParent(props: {
    parent?: PixiParent;
    children?: PixiNode;
}): JSX.Element;
