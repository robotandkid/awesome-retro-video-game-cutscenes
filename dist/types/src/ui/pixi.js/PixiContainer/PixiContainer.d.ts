import { PixiParent, PixiNode } from "../pixi";
export declare function PixiContainer(props: {
    offsetY?: number;
} & {
    parent?: PixiParent;
    children?: PixiNode;
}): JSX.Element;
