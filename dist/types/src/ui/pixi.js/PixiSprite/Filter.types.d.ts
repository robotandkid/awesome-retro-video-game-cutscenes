declare type BaseFilter = {
    /**
     * Currently unused
     */
    target?: "background";
};
export declare const simpleFilterType: readonly ["crt", "dropshadow", "outline", "glitch", "glow", "godray", "kawaseblur", "motionblur", "oldfilm", "reflection", "tiltshift", "zoomblur"];
export interface SimpleFilter extends BaseFilter {
    filter: typeof simpleFilterType[number];
}
export declare const configurableFilterType: readonly ["dot", "pixelate"];
declare type ConfigurableFilterType = typeof configurableFilterType[number];
export interface DotFilter extends BaseFilter {
    filter: Extract<ConfigurableFilterType, "dot">;
    /**
     * Defaults to 1.
     * @TJS-type number
     * @minimum 0
     * @maximum 1
     */
    scale?: number;
    /**
     * The radius. Defaults to 1.
     * @TJS-type number
     * @minimum 0
     */
    radius?: number;
}
export interface PixelateFilter extends BaseFilter {
    filter: Extract<ConfigurableFilterType, "pixelate">;
    /**
     * The width/height of the pixels. Defaults to 3.
     * @minimum 0
     * @TJS-type number
     */
    size?: number;
}
export declare type ConfigurableFilter = DotFilter | PixelateFilter;
export declare const filterWithNoDefaultsType: readonly ["colorreplace", "adjustment"];
declare type FilterWithNoDefaultsType = typeof filterWithNoDefaultsType[number];
export interface ColorReplaceFilter extends BaseFilter {
    filter: Extract<FilterWithNoDefaultsType, "colorreplace">;
    /**
     * HTML color (ex: 0xfffff)
     * @TJS-type number
     */
    originalColor: number;
    /**
     * HTML color (ex: 0xfffff)
     * @TJS-type number
     */
    newColor: number;
    /**
     * The tolerance. The lower the more accurate.
     *
     * @minimum 0
     * @maximum 1
     * @TJS-type number
     */
    epsilon: number;
}
export interface AdjustmentFilter extends BaseFilter {
    filter: Extract<FilterWithNoDefaultsType, "adjustment">;
    /**
     * The amount of luminance. Defaults to 1
     * @TJS-type number
     * @minimum 0
     */
    gamma?: number;
    /**
     * The amount of color saturation. Defaults to 1
     * @TJS-type number
     * @minimum 0
     */
    saturation?: number;
    /**
     * The amount of contrast. Defaults to 1
     * @TJS-type number
     * @minimum 0
     */
    contrast?: number;
    /**
     * The overall brightness. Defaults to 1
     * @TJS-type number
     * @minimum 0
     */
    brightness?: number;
    /**
     * Defaults to 1
     * @TJS-type number
     * @minimum 0
     */
    red?: number;
    /**
     * Defaults to 1
     * @TJS-type number;
     * @minimum 0
     */
    green?: number;
    /**
     * Defaults to 1
     * @TJS-type number;
     * @minimum 0
     */
    blue?: number;
    /**
     * The overall alpha amount. Defaults to 1.
     * @TJS-type number;
     * @minimum 0
     * @maximum 1
     */
    alpha?: number;
}
declare type FilterWithNoDefaults = ColorReplaceFilter | AdjustmentFilter;
export declare const supportedFilterType: readonly ["crt", "dropshadow", "outline", "glitch", "glow", "godray", "kawaseblur", "motionblur", "oldfilm", "reflection", "tiltshift", "zoomblur", "dot", "pixelate", "colorreplace", "adjustment"];
export declare type SupportedFilterType = SimpleFilter["filter"] | ConfigurableFilterType | FilterWithNoDefaultsType;
export declare type Filter = SimpleFilter | ConfigurableFilter | FilterWithNoDefaults;
export {};
