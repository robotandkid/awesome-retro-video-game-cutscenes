import { Sprite } from "pixi.js";
import { CmdFilter } from "../../../DialogEngine/dialog.types";
import { InternalPixiResourceMetaData, PixiElementProps } from "../pixi";
export interface SpriteData {
    resource: InternalPixiResourceMetaData;
    sprite: Sprite;
    computedHeight: number;
    computedWidth: number;
}
interface PixiSpriteOpts {
    path: string | undefined;
    filter?: CmdFilter | Array<CmdFilter>;
    containerWidth: number;
    containerHeight: number;
}
/**
 * Loads image and resizes it (to scale) to fit
 * inside the container
 *
 * @param props
 */
export declare function useLoadImage(props: PixiSpriteOpts): SpriteData | undefined;
export declare function PixiSprite(props: PixiSpriteOpts & PixiElementProps): JSX.Element;
export {};
