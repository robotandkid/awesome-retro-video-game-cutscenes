import { PixiChild, PixiParent } from "../pixi";
export declare const AddToParentOnMount: (props: {
    child: PixiChild | undefined;
    parent: PixiParent | undefined;
}) => null;
