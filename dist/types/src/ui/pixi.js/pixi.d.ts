import "pixi-sound";
import { Application, Container, ILoaderResource } from "pixi.js";
import React from "react";
import { Resources } from "./loaders/useLoadResources";
export interface PixiResourceMetaData {
    type: "image" | "sound";
    path: string;
    /**
     * Must be globally unique
     */
    id?: string;
}
export interface InternalPixiResourceMetaData {
    id: string;
    path: string;
}
export declare function internalPixiResourceMetaData(props: PixiResourceMetaData): InternalPixiResourceMetaData;
export declare type PixiNode = React.ReactNode | React.ReactNodeArray;
export interface PixiAppOpts {
    width: number;
    height: number;
    resolution?: number;
    backgroundColor?: number;
    resources: Resources | undefined;
    onAppInit?: () => void;
    onCanvasFocus?: () => void;
    onCanvasClick?: () => void;
    exportToVideo?: boolean;
    children?: PixiNode;
}
export declare const PixiAppTickerContext: React.Context<import("pixi.js").Ticker | undefined>;
export declare const PixiCanvasRefContext: React.Context<HTMLCanvasElement | undefined>;
export declare const PixiLoaderResourcesContext: React.Context<Partial<Record<string, ILoaderResource>> | undefined>;
export declare function PixiApp(props: PixiAppOpts): JSX.Element;
export declare type PixiParent = Application | Container;
export declare type PixiChild = Container;
/**
 * The PixiApp and PixiContainer will inject themselves
 * to any direct descendants.
 *
 * Add this property if you need access to the pixi parent.
 */
export declare type PixiElementProps = {
    parent?: PixiParent;
};
export declare function isApplication(cmp: PixiParent | undefined): cmp is Application;
export declare function isContainer(cmp: PixiParent | undefined): cmp is Container;
