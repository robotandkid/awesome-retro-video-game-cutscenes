declare const _default: {
    "a single line": JSX.Element;
    "duplicate lines": JSX.Element;
    "multiple lines": JSX.Element;
};
export default _default;
