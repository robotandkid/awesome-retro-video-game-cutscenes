interface NextEventProps {
    onNextEvent: () => void;
    onNextEventDoublePress?: () => void;
}
interface PreviousEventProps {
    onPreviousEvent: () => void;
    onPreviousEventDoublePress?: () => void;
}
declare type EventingProps = (NextEventProps & Partial<PreviousEventProps>) | (Partial<NextEventProps> & PreviousEventProps) | (NextEventProps & PreviousEventProps);
export declare function AppEventing(props: EventingProps): null;
export {};
