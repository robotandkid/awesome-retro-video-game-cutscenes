declare const _default: {
    hookRenderCountTest: JSX.Element;
    naiveProviderRenderCountTest: JSX.Element;
    providerRenderCountTest: JSX.Element;
};
export default _default;
