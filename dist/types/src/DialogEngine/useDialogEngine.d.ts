import { PixiResourceMetaData } from "../ui/pixi.js/pixi";
import { DialogNode, InternalCommand } from "./dialog.types";
export interface DialogEngineProps {
    nodes: DialogNode[];
}
export declare type DialogEngineOutput = InternalCommand;
export declare type SubscribeCommandCallback = (cmd: InternalCommand) => void;
export declare type UnsubscribeCommand = () => void;
export interface DialogEngineInterface {
    current: {
        nodeTitle: string;
        cmdNodeIndex: number;
        cmd: InternalCommand;
    };
    resources: PixiResourceMetaData[];
    callbacks: {
        advance(): void;
        restartNodeOrPreviousNode(): void;
        restart(): void;
    };
}
/**
 * The idea here is that we can swap this out for something like yarn dialog
 */
export declare function useDialogEngine(props: DialogEngineProps): DialogEngineInterface;
