import { Filter } from "../ui/pixi.js/PixiSprite/Filter.types";
export declare type CmdFilter = Filter;
export declare type CmdSayLine = {
    say: string;
    actor: "narrator" | string;
};
declare type CmdDrawBg = {
    "draw-bg": string;
    name?: string;
};
declare type CmdJump = {
    jump: string;
};
interface CmdDelay {
    delayInMs: number;
}
export declare type Command = CmdSayLine | CmdDrawBg | CmdJump | CmdFilter | CmdDelay;
/**
 * This is the more user-friendly interface
 */
export interface DialogNode {
    title: string;
    commands: Command[];
}
export declare type InternalCmdFilter = CmdFilter & {
    type: "filter";
};
export declare type InternalCommand = (CmdSayLine & {
    type: "say-line";
}) | (CmdDrawBg & {
    type: "draw-bg";
}) | (CmdJump & {
    type: "jump";
}) | (CmdDelay & {
    type: "delay";
}) | {
    type: "end";
} | InternalCmdFilter;
export interface InternalDialogNode {
    title: string;
    commands: InternalCommand[];
}
export declare function commandToInternalCommand(cmd: Command): InternalCommand;
export {};
