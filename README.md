# awesome-react-cutscenes

A React library for creating old-school video-game cutscenes using WebGL and Canvas.

## Installation

```bash
# dependencies
yarn add react react-dom pixi.js pixi-filters
yarn add pixi-sound # optional
```

## Development

```bash
yarn cosmos
```
